# Motorlog

Application de suivi des travaux et entretiens sur véhicules anciens ou non

#### Etat d'avancement -> étude de besoin

## Besoin

Avoir dans une applciation mobile un index de suivi des entretiens réalisés sur un véhicule ancien. L'objectif est de tenir un **registre d'entretien** exportable (MD/PDF ?) et importable. A terme il faudrai pouvoir stocker les factures de garagistes, ainsi que des photos des réparations.
On peut aussi imaginer un system de rappels d'entretiens réguliers comme les vidanges tous les X kilomètres et une todo liste des futurs chantiers.

L'application doit aussi être en mesure de gérer plusieurs véhicules.

Pourquoi pas sortir des statistiques pertinentes sur l'évolution de la voiture.

## Spécifications de l'application

 > * Codée avec le framework ionic (angular)
 > * Codée en anglais
 > * Clean code
 > * Hors ligne
 > * e2e testing avec Cypress
 > * Pas de tests unitaires, juste du e2e 

## PMV

- [ ] E2E Testing
- [ ] Véhicule
	- [x] CRUD sans sous objets
  - [ ] CRUD Sous objets
		- [ ] Photos
		- [x] Kilométrages
		- [ ] Rappels
		- [ ] Carnet d'entretien
		- [ ] Caractéristiques
- [ ] SplashScreen
- [ ] Logo
- [ ] Export des données
- [ ] Import de données
- [ ] Affichage statistiques (en fonction du temps)
		

### Modèles de données

###### Véhicule

| Véhicule      			| Attribut      	| Type 	 	|
|---------------------|-----------------| --------|
| ID	 								| id	 						| Number 	|
| Type 								| type 						| String 	|
| Nom  								|	name 						| String 	|
| Marque 							| brand 					| String 	|
| Modèle 							| model 					| String 	|
| Année 							| year 						| Number 	|
| Kilimétrages (km)		| mileages				| Object 	|
| DateAcquisition 		| acquisitionDate	|  Date 	|
| Photos  						| photos					| Object 	|
| Rappels 						| reminds 				| Object	|
| Carnet d'entretien 	| logBook 				| Object  |
| Caractéristiques		| lineaments			| Object

###### Rappel

| Rappel      			  | Attribut      	| Type 	 	|
|---------------------|-----------------| --------|
| ID	 								| id	 						| Number 	|
| Type 								| type 						| String 	|
| Récuprence (km) 		| recurencyKM			| Number 	|

###### Kilométrage

| Kilométrage  			  | Attribut      	| Type 	 	|
|---------------------|-----------------| --------|
| Date 								| setAt						| Date	 	|
| Nombre					 		| value						| Number 	|


###### Entretien

| Entretien  			  	| Attribut      	| Type 	 	|
|---------------------|-----------------| --------|
| ID	 								| id	 						| Number 	|
| Type 								| type						| String 	|
| Libelle					 		| libelle					| String 	|
| Date 								| setAt						| Date	 	|
| Photos					 		| photos					| Object 	|
| Documents				 		| documents				| Object 	|

###### Caractéristiques

| Caractéristique	  	| Attribut      	| Type 	 	|
|---------------------|-----------------| --------|
| ID	 								| id	 						| Number 	|
| Type 								| type						| String 	|
| Libelle					 		| libelle					| String 	|
| Valeur					 		| value						| String 	|

#### Notes relatives aux modèles

Caractéristiques :
Il serait intéressant de connaitres certaines informations du genre vitesse max effective, températures de chauffes, particularité de carosseries etc ..

Entretien :
Dans cette optique, un entretien est une action/réparation réalisée sur la voiture avec les documents/photos à l'appuie et dates.

Rappel : 
A chaque enregistrement d'un nouveau kilométrage, on pourrait vérifier les rappel et mettre des popup de rappel