import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VehiculePage } from './vehicule.page';

const routes: Routes = [
  {
    path: '',
    component: VehiculePage
  },
  {
    path: 'edition-vehicule',
    loadChildren: () => import('./edition-vehicule/edition-vehicule.module').then( m => m.EditionVehiculePageModule)
  },
  {
    path: 'details-vehicule',
    loadChildren: () => import('./card-vehicule/card-vehicule.module').then( m => m.CardVehiculePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VehiculePageRoutingModule {}
