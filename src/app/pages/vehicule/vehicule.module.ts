import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VehiculePageRoutingModule } from './vehicule-routing.module';

import { VehiculePage } from './vehicule.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VehiculePageRoutingModule,
    SharedModule
  ],
  declarations: [VehiculePage]
})
export class VehiculePageModule {}
