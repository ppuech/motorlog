import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditionVehiculePage } from './edition-vehicule.page';

describe('EditionVehiculePage', () => {
  let component: EditionVehiculePage;
  let fixture: ComponentFixture<EditionVehiculePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditionVehiculePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditionVehiculePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
