import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Vehicule } from 'src/app/models/vehicule/vehicule.model';
import { VehiculeService } from 'src/app/models/vehicule/vehicule.service';

@Component({
  selector: 'app-edition-vehicule',
  templateUrl: './edition-vehicule.page.html',
  styleUrls: ['./edition-vehicule.page.scss'],
})
export class EditionVehiculePage implements OnInit {

  vehiculeForm: FormGroup;
  newVehicule: Vehicule = new Vehicule();
  isUpdating = false;


  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private vehiculeService: VehiculeService
    ) {
      this.activatedRoute.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          const updateVehicule =  this.router.getCurrentNavigation().extras.state.vehicule;
          if (updateVehicule) {
            this.newVehicule = updateVehicule;
            this.isUpdating = true;
          }
        }
      });
     }

  ngOnInit() {
    this.vehiculeForm = new FormGroup({
      type: new FormControl(this.newVehicule.type || '', [Validators.required]),
      name: new FormControl(this.newVehicule.name || '', [Validators.required]),
      brand: new FormControl(this.newVehicule.brand || '', [Validators.required]),
      model: new FormControl(this.newVehicule.model || '', [Validators.required]),
      year: new FormControl(this.newVehicule.year || '', [Validators.required]),
      acquisitionDate: new FormControl(this.newVehicule.acquisitionDate || '', [Validators.required]),
      roulant: new FormControl(this.newVehicule.roulant || false)
   });
  }

  onSubmit(): void {
    if (this.vehiculeForm.valid) {
      this.newVehicule = Object.assign(this.newVehicule, this.vehiculeForm.value);
      if(this.isUpdating) {
        this.vehiculeService.update(this.newVehicule);
      } else {
        this.vehiculeService.create(this.newVehicule);
      }
      this.router.navigateByUrl('/vehicule');
    }
  }

}
