import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditionVehiculePageRoutingModule } from './edition-vehicule-routing.module';

import { EditionVehiculePage } from './edition-vehicule.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditionVehiculePageRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [EditionVehiculePage]
})
export class EditionVehiculePageModule {}
