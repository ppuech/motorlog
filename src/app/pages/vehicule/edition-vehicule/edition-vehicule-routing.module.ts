import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditionVehiculePage } from './edition-vehicule.page';

const routes: Routes = [
  {
    path: '',
    component: EditionVehiculePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditionVehiculePageRoutingModule {}
