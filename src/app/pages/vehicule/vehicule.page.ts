import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Vehicule } from 'src/app/models/vehicule/vehicule.model';
import { VehiculeService } from 'src/app/models/vehicule/vehicule.service';

@Component({
  selector: 'app-vehicule',
  templateUrl: './vehicule.page.html',
  styleUrls: ['./vehicule.page.scss'],
})
export class VehiculePage implements OnInit, OnDestroy {

  public vehicules: Vehicule[];
  private ngUnSubscribe$ = new Subject();

  constructor(
    public router: Router,
    private vehiculeService: VehiculeService
  ) {}

  ngOnInit() {
    this.vehiculeService.selectAll().pipe(takeUntil(this.ngUnSubscribe$)).subscribe( (vehiculesStockes: Vehicule[]) => {
      this.vehicules = vehiculesStockes;
    });
  }

  ngOnDestroy() {
    this.ngUnSubscribe$.next();
    this.ngUnSubscribe$.complete();
  }

  /**
   * Redirige vers la page d'edition d'un véhicule
   */
  editionVehicule(): void {
    this.router.navigateByUrl('/vehicule/edition-vehicule');
  }

  goDetails(vehicule: Vehicule): void {
    const navigationExtras: NavigationExtras = {
      state: {
        vehicule
      }
    };
    this.router.navigate(['/vehicule/details-vehicule'], navigationExtras);
  }
}
