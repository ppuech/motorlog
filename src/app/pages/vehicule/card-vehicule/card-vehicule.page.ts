import { Component } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Mileage } from 'src/app/models/various/mileage.model';
import { Vehicule } from 'src/app/models/vehicule/vehicule.model';
import { VehiculeService } from 'src/app/models/vehicule/vehicule.service';

@Component({
  selector: 'app-card-vehicule',
  templateUrl: './card-vehicule.page.html',
  styleUrls: ['./card-vehicule.page.scss'],
})
export class CardVehiculePage {

  vehicule = new Vehicule();

  constructor(
    public router: Router,
    private activatedRoute: ActivatedRoute,
    private vehiculeService: VehiculeService,
    private alertController: AlertController
    ) {

      this.activatedRoute.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.vehicule = this.router.getCurrentNavigation().extras.state.vehicule;
          if (!this.vehicule) {
            this.router.navigate(['/vehicule']);
          }
        } else {
          this.router.navigate(['/vehicule']);
        }
      });

    }

  async presentSetMileage() {
    const alert = await this.alertController.create({
      header: 'Entrez un kilométrage :',
      inputs: [
        {
          name: 'mileage_value',
          type: 'number'
        }
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          handler: () => {
            console.log('Ajout de kilométrage annulé');
          }
        }, {
          text: 'Ok',
          handler: (data: any) => {
            if (!this.vehicule.mileages) {
              this.vehicule.mileages = [];
            }
            this.vehicule.mileages = [...this.vehicule.mileages, new Mileage(data.mileage_value, new Date())];
            this.vehiculeService.update(this.vehicule);
          }
        }
      ]
    });

    await alert.present();
  }

  /**
   * Méthode de redirection vers la page d'édition d'un véhicule
   */
  editVehicule(): void {
    const navigationExtras: NavigationExtras = {
      state: {
        vehicule: this.vehicule
      }
    };
    this.router.navigate(['/vehicule/edition-vehicule'], navigationExtras);
  }

  /**
   * Méthode de suppression d'un véhicule
   */
  deleteVehicule(): void {
    this.vehiculeService.remove(this.vehicule);
    this.router.navigate(['/vehicule']);
  }
}
