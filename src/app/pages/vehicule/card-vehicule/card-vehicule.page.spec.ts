import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CardVehiculePage } from './card-vehicule.page';

describe('CardVehiculePage', () => {
  let component: CardVehiculePage;
  let fixture: ComponentFixture<CardVehiculePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardVehiculePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CardVehiculePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
