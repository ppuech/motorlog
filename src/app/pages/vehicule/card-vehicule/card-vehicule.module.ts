import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardVehiculePageRoutingModule } from './card-vehicule-routing.module';

import { CardVehiculePage } from './card-vehicule.page';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardVehiculePageRoutingModule,
    SharedModule
  ],
  declarations: [CardVehiculePage]
})
export class CardVehiculePageModule {}
