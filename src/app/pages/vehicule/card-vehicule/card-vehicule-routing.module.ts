import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CardVehiculePage } from './card-vehicule.page';

const routes: Routes = [
  {
    path: '',
    component: CardVehiculePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardVehiculePageRoutingModule {}
