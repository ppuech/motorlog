import { NgModule } from '@angular/core';
import { VehiculeService } from './vehicule/vehicule.service';

@NgModule({
  imports: [
  ],
  exports: [
  ],
  declarations: [
  ],
  providers: [
    VehiculeService
  ]
})
export class ModelsModule { }
