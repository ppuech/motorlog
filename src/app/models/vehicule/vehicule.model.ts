import { Mileage } from '../various/mileage.model';

export class Vehicule {

  id: number;
  type: string;
  name: string;
  brand: string;
  model: string;
  year: number;
  roulant: boolean;
  acquisitionDate: Date;

  mileages: Mileage[];
  photos: any[];
  reminds: any[];
  logBook: any[];
  lineaments: any[];

  constructor() {}
}
