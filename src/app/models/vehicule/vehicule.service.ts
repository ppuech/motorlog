import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject } from 'rxjs';
import { take } from 'rxjs/operators';
import { AppServiceInterface } from 'src/app/shared/interfaces/service.interface';
import { Vehicule } from './vehicule.model';

@Injectable()
export class VehiculeService implements AppServiceInterface<Vehicule>{

  private readonly SCHEMA_VALUE = 'vehicule';
  public items$: BehaviorSubject<Vehicule[]> = new BehaviorSubject([]);

  constructor(private storage: Storage) {
    this.init();
   }

  public create(item: Vehicule): void {
    this.items$.pipe(take(1)).subscribe( itemsLocal => {

      // Si aucun chef n'est enregsitré, on crée/recrée l'id 1
      if (itemsLocal[itemsLocal.length - 1]) {
        item.id = itemsLocal[itemsLocal.length - 1].id + 1;
      } else {
        item.id = 1;
      }
      itemsLocal.push(item);

      this.items$.next(itemsLocal);
      this.storage.set(this.SCHEMA_VALUE, itemsLocal);

    });
  }

  public update(item: Vehicule): void {
    this.items$.pipe(take(1)).subscribe( itemsLocal => {
      const itemLocal = itemsLocal.find(it => it.id === item.id);
      const index = itemsLocal.indexOf(itemLocal);
      if (index > -1) {
        itemsLocal[index] = item;
      }
      this.items$.next(itemsLocal);
      this.storage.set(this.SCHEMA_VALUE, itemsLocal);
    });
  }

  public select(id: number): Vehicule {
    return this.items$.getValue().find(
        chef => chef.id === id
    );
  }

  public selectAll(): BehaviorSubject<Vehicule[]> {
    this.storage.get(this.SCHEMA_VALUE).then(unitesFromDB => {
     this.items$.next(unitesFromDB);
    });
    return this.items$;
  }

  public remove(item: Vehicule) {
    this.items$.pipe(take(1)).subscribe( itemsLocal => {
      const index = itemsLocal.indexOf(item);
      if (index > -1) {
        itemsLocal.splice(index, 1);
     }
      this.items$.next(itemsLocal);
      this.storage.set(this.SCHEMA_VALUE, itemsLocal);
    });
  }

  public init() {
    this.storage.get(this.SCHEMA_VALUE).then(itemsFromDB => {
      if (!itemsFromDB) {
        this.storage.set(this.SCHEMA_VALUE, this.items$.value);
      } else {
        this.items$.next(itemsFromDB);
      }
    });
  }

  public clear(): void {
    this.items$.next([]);
    this.storage.set(this.SCHEMA_VALUE, []);
  }

}
