import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Vehicule } from 'src/app/models/vehicule/vehicule.model';

export interface AppServiceInterface<T> {

  items$: BehaviorSubject<T[]>;

  /**
   * Endpoint de création d'un item
   * Les items en local sont récupérées, on y ajoute nouveau
   * et on met à jour la liste en base
   * @param item l'item à stocker
   */
  create(item: T): void;

  /**
   * Endpoint de modification d'un item
   * Les items en local sont récupérées, on modifie l'item ciblé
   * et on met à jour la liste en base
   * @param item l'item à modifier
   */
  update(item: T): void;

  /**
   * Endpoint de récupération d'un item
   * @param id l'id de l'item à récupérer
   * @return un item
   */
  select(id: number): T;

  /**
   * Endpoint de récupération de tous les items en base
   * Les items récupérés sont stockés dans un observable en tant qu'attribut de service.
   * @return une liste d'item sous forme d'observable
   */
  selectAll(): BehaviorSubject<T[]>;

  /**
   * Endpoint de suppression d'un item
   * @param item l'item à supprimer
   */
  remove(item: Vehicule): void;

  /**
   * Endpoint d'initialisation du schema de l'item s'il n'existe pas encore
   */
  init(): void;

  /**
   * Endpoint de suppression de tous les items stockés
   */
  clear(): void;
}
