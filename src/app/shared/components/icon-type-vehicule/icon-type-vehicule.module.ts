import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { IconTypeVehiculeComponent } from './icon-type-vehicule.component';

@NgModule({
  imports: [IonicModule, CommonModule],
  declarations: [IconTypeVehiculeComponent],
  exports: [IconTypeVehiculeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class IconTypeVehiculeModule { }
