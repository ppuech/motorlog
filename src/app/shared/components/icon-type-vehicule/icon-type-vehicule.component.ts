import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-icon-type-vehicule',
  template: `
    <ion-icon *ngIf="typeVehicule == 'voiture'" slot="start" name="car"></ion-icon>
    <ion-icon *ngIf="typeVehicule == 'moto'" slot="start" src="/assets/moto.svg"></ion-icon>
    <ion-icon *ngIf="typeVehicule == 'mobylette'" slot="start" src="/assets/mobylette.svg"></ion-icon>
    <ion-icon *ngIf="typeVehicule == 'velo'" slot="start" name="bicycle"></ion-icon>
  `,
  styleUrls: ['./icon-type-vehicule.component.scss'],
})
export class IconTypeVehiculeComponent {

  @Input()
  typeVehicule: TypeVehicule;

}
