import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-fab-button',
  templateUrl: './fab-button.component.html',
  styleUrls: ['./fab-button.component.scss'],
})
export class FabButtonComponent {

  @Input()
  public color: string;

  @Input()
  public icone: string;

  @Output()
  public action = new EventEmitter<void>();

  constructor() { }

}
