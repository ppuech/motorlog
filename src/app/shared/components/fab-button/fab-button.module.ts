import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { FabButtonComponent } from './fab-button.component';

@NgModule({
  imports: [IonicModule, CommonModule],
  declarations: [FabButtonComponent],
  exports: [FabButtonComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class FabButtonModule { }
