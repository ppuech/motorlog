import { NgModule } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { FabButtonModule } from './components/fab-button/fab-button.module';
import { IconTypeVehiculeModule } from './components/icon-type-vehicule/icon-type-vehicule.module';

@NgModule({
  imports: [
    FabButtonModule,
    IconTypeVehiculeModule,
    MatIconModule
  ],
  exports: [
    FabButtonModule,
    IconTypeVehiculeModule
  ]
})
export class SharedModule {}

